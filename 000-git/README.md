O git é um versionador de código usado muito pela comunidade DEV porem com o avanço da tecnologia e o uso em grande escala de IaC (Infraestructure as Code) passou a ser usado tambem pela galera de Operações/Infra.

No mundo infra, podemos por exemplo versionar as configurações a serem aplicadas por outras ferramentas, como o ansible, no dns; ou até mesmo versionar os codigos usados em ferramentas de automação como o terraform; tambem codigos usados para CI/CD no jenkins.


Alguns comando de destacam:

$ git init 
  - para iniciar um repositório git.
$ git status
  - para verificar em que estagio estão seu arquivos deste diretorio git
$ git add
  - Muda os arquivos modificados de estágio
$ git commit
  - comita as aletações e prepara os arquivos para serem enviados para o repositório
$ git push 
  - envia efetivamente os arquivos para o repositório
$ git checkout
  - Troca a branch de trabalho
