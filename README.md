# Aulas Cloud / DEVOPS

## Comandos iniciais usados após a criação do repositório no GITLAB

*Configuração global do Git*

*git config --global user.name "Hernani Soares Neto"*

*git config --global user.email "soaresnetoh@gmail.com"*

*Criar um novo repositório*

*git clone git@gitlab.com:soaresnetoh/aulas.git*

*cd aulas*

*touch README.md*

*git add README.md*

*git commit -m "add README"*

*git push -u origin master*


##Este repostório contem as tarefas feitas durante as aulas de DEVOPS / AWS

**Diretório:** */000-git*

Conteudo: Instruções sobre o uso do git


**Diretório:** */001-docker*

Conteudo: Instruções sobre o uso do docker


**Diretório:** */002-cloudformation*

Conteudo: IaC Cloudformation para criação de:
```
* 1 VPC 
* 4 SUBNET - 2 PUBLICA / 2 PRIVADAS
* 1 IG - INTERNET GATEWAY
* 2 RT - ROUTE TABLE - 1 PUBLICA / 1 PRIVADA
* 2 NACL - NETWORKACL - 1 PUBLICA / 1 PRIVADA
```
**INBOUND PUBLICA                                                                   
|   |               |                   |               |                    |     |
|---|---------------|-------------------|---------------|--------------------|-----|
|100|	HTTP (80)	|TCP (6)	        |80	            |0.0.0.0/0	         |Allow|
|200|	HTTPS (443)	|TCP (6)	        |443	        |0.0.0.0/0	         |Allow|
|300|	SSH (22)	|TCP (6)         	|22	            |177.12.41.0/24	     |Allow|
|310|	Custom TCP	|TCP (6)	        |631	        |0.0.0.0/0	         |Allow|
|400|	Custom TCP	|TCP (6)	        |1024 - 65535	|0.0.0.0/0	         |Allow|
|410|	Custom UDP	|UDP (17)	        |1024 - 65535	|0.0.0.0/0	         |Allow|
|500|	All ICMP -  |IPv4	ICMP (1)	|All	        |177.12.41.0/24	     |Allow|
|*  |	All traffic	|All                |All            |	0.0.0.0/0        |Deny |

**OUTBOUND PUBLICA                                                                  
|   |                               |           |               |            |     |
|---|-------------------------------|-----------|---------------|------------|-----|
|100|	HTTP (80)	                |TCP (6)	|80	            |0.0.0.0/0	 |Allow|
|200|	HTTPS (443)             	|TCP (6)	|443	        |0.0.0.0/0	 |Allow|
|300|	SSH (22)	                |TCP (6)	|22             |0.0.0.0/0	 |Allow|
|310|	Custom TCP              	|TCP (6)	|631	        |0.0.0.0/0	 |Allow|
|330|	Custom TCP	                |TCP (6)	|123	        |0.0.0.0/0	 |Allow|
|400|	Custom TCP	                |TCP (6)	|1024 - 65535	|0.0.0.0/0	 |Allow|
|410|	Custom UDP              	|UDP (17)	|1024 - 65535	|0.0.0.0/0	 |Allow|
|500|	All ICMP - IPv4	            |ICMP (1)	|All	        |0.0.0.0/0	 |Allow|
|*  |	All traffic	                |All	    |All	        |0.0.0.0/0	 |Deny |

**INBOUND PRIVADA                                                                   
|       |                       |           |               |                |     |
|-------|-----------------------|-----------|---------------|----------------|-----|
|100	|HTTP (80)	            |TCP (6)	|80	            |0.0.0.0/0	     |Allow|
|200	|HTTPS (443)	        |TCP (6)	|443	        |0.0.0.0/0	     |Allow|
|300	|SSH (22)	            |TCP (6)	|22	            |177.12.41.0/24	 |Allow|
|310	|Custom TCP	            |TCP (6)	|631        	|0.0.0.0/0	     |Allow|
|320	|Custom TCP	            |TCP (6)	|27017	        |10.192.0.0/16	 |Allow|
|330	|MySQL/Aurora (3306)	|TCP (6)	|3306       	|10.192.0.0/16	 |Allow|
|400	|Custom TCP         	|TCP (6)	|1024 - 65535	|0.0.0.0/0	     |Allow|
|410	|Custom UDP	            |UDP (17)	|1024 - 65535	|0.0.0.0/0	     |Allow|
|500	|All ICMP - IPv4	    |ICMP (1)	|All        	|0.0.0.0/0	     |Allow|
|*	    |All traffic	        |All	    |All        	|0.0.0.0/0	     |Deny |

**OUTBOUND PRIVADA                                                                  
|       |                   |               |               |                |     |
|-------|-------------------|---------------|---------------|----------------|-----|
|100	|HTTP (80)	        |TCP (6)	    |80         	|0.0.0.0/0	     |Allow|
|200	|HTTPS (443)	    |TCP (6)	    |443        	|0.0.0.0/0	     |Allow|
|300	|SSH (22)	        |TCP (6)        |22	            |10.192.0.0/16	 |Allow|
|310	|Custom TCP	        |TCP (6)        |631	        |0.0.0.0/0	     |Allow|
|320	|Custom TCP	        |TCP (6)        |27017      	|10.192.0.0/16	 |Allow|
|330	|MySQL/Aurora (3306)|TCP (6)	    |3306	        |10.192.0.0/16	 |Allow|
|400	|Custom TCP	        |TCP (6)	    |1024 - 65535	|0.0.0.0/0	     |Allow|
|410	|Custom UDP	        |UDP (17)	    |1024 - 65535	|0.0.0.0/0	     |Allow|
|500	|All ICMP - IPv4	|ICMP (1)	    |All	        |0.0.0.0/0	     |Allow|
|*  	|All traffic	    |All	        |All	        |0.0.0.0/0	     |Deny |



|   |   |   |   |   |
|---|---|---|---|---|
|   |   |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |