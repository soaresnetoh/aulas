Aqui vou criar uma maquina virtual no Virtual Box, com o Minimal Ubuntu 16.04.2 install usando o Vagrant, que usa código para esta criação.

Está maquina servirá para testes.

Caso precise acessar pelo ip externo: 192.168.0.77

Comandos para usar

Cria e inicia a VM
$ vagrant up

acessa a VM por ssh
$ vagrant ssh

Desliga a VM
$vagrant halt

Destroy a VM - se usar este, ao criar novamente perderá tudo que foi instalado... sugestão: criar um script para que o que foi feito seja automaticamente instalado novamente
$ vagrant destroy



