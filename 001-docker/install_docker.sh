#!/bin/bash

echo "############### Remove algum docker que já esteja instalado"
sudo apt-get remove docker docker-engine docker.io containerd runc

echo "############### Atualiza pacotes"
sudo apt-get update

echo "############### Instala dependencias"
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

echo "############### Assinatura"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "############### Coloca o repositorio necessário"
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "############### Instala o docker"
sudo apt-get update &&  sudo apt-get install docker-ce docker-ce-cli containerd.io -y

echo "############### Coloca o usuario com permissão no docker - necessário fechar e abrir o terminal"
sudo usermod -aG docker $(whoami)

echo "############### Cria a rede docker"
docker network create network1

echo "############### Criar o container MySql  - com persistencia no diretorio atual / cms"
docker run --network network1 --name mysqldb -e MYSQL_ROOT_PASSWORD=joao01 -e MYSQL_DATABASE=wordpress -v $(pwd)/cms/mysql:/var/lib/mysql -d mysql

echo "############### Cria o container wdo Wordpress - com persistencia no diretorio atual / cms"
docker run --network network1 --name wordpress -e WORDPRESS_DB_HOST=mysqldb:3306 -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=joao01 -e WORDPRESS_DB_NAME=wordpress -p 8080:80 -v $(pwd)/cms/wordpress:/var/www/html -d wordpress

echo "############### Acesse : http://<IP_DA_MAQUINA>:8080"