Para instalar o Docker acesse o site https://docs.docker.com/get-docker e acesso seu sistema operacional

No Linux Ubuntu:

$ sudo apt-get remove docker docker-engine docker.io containerd runc

$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

$ echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

$  sudo apt-get update &&  sudo apt-get install docker-ce docker-ce-cli containerd.io -y


Após isso podemos passar para a criação do container docker

alguns comandos

$ docker --version

$ sudo usermod -aG docker $(whoami)

apos isso, fechar o terminal e acessar novamente

$ docker run
 - roda um container baseado na imagem informada. 
 - Ex: $ docker run hello-world

iniciando um container ubuntu e já entrando nele. o Bash é o comando para acessar o container

$ docker run -it --name MeuUbuntu ubuntu bash

outra forma de acessa o container é depois de criado, usar o comando:

$ docker exec -it <nome do container ou ID do container> /bin/bash
 

para sair do container 

$ exit ou <CTRL>+d


para remover o container 

$ docker stop <nome do container ou ID do container>
$ docker rm <nome do container ou ID do container>


Exemplo uso de docker

Instalando um webserver
$ docker run -d --name webserver -p 8080:80 nginx

Parametro : 
   -d -> roda em background
   --name -> nomea o container
   -p -> mapea porta do <host>:<container>

para testar :
$ curl 192.168.0.77:8080


Rodando algum comando dentro co container

$ docker exec <nome do container ou ID do container> comando
$ docker exec -it <nome do container ou ID do container> /bin/bash



Criando uma imagem a partir do container já parameterizado

$ docker commit <container> <NovoContainer>
$ docker commit webserver mywebsite
$ docker image ls
$ docker run -d --name webserver -p 8080:80 mywebsite

o comando para inspecionar o container e verificar as configurações

$ docker inspect <nome do container ou ID do container>


Persistindo com Volumes

Usar o parametro -v e colocar a pasta a ser persistida
$ docker run -d -v /usr/share/nginx/html --name webserver -p 8080:80 mywebsite

Persistindo com Mapeamento

$ docker run -d -v <pasta do host>:<pasta do container> --name webserver -p 8080:80 mywebsite
Ex:
$ docker run -d -v /home/vagrant/website:/usr/share/nginx/html --name webserver -p 8080:80 mywebsite

Comunicação entre os containers

Por padrão os container não tem contato

Para que isso ocorra temos que criar uma rede para que os container sejam associadas a esta rede

$ docker network create <nome da rede>
$ docker network create network1

para teste usar o container do mysql e do wordpress

simples de conseguir os parametros na documentação

mysql - https://hub.docker.com/_/mysql

parametros obrigatórios:

MYSQL_ROOT_PASSWORD
para o cenário será usado tambem o parametro MYSQL_DATABASE para que o banco do wordpress seja criado tambem

$ docker run --network network1 --name mysqldb -e MYSQL_ROOT_PASSWORD=joao01 -e MYSQL_DATABASE=wordpress -d mysql





wordpress - https://hub.docker.com/_/wordpress

$ docker run --network network1 --name wordpress -e WORDPRESS_DB_HOST=mysqldb:3306 -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=joao01 -e WORDPRESS_DB_NAME=wordpress -p 8080:80 -d wordpress


Persistindo 

$ docker run --network network1 --name mysqldb -e MYSQL_ROOT_PASSWORD=joao01 -e MYSQL_DATABASE=wordpress -v /home/vagrant/cms/mysql:/var/lib/mysql -d mysql


$ docker run --network network1 --name wordpress -e WORDPRESS_DB_HOST=mysqldb:3306 -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=joao01 -e WORDPRESS_DB_NAME=wordpress -p 8080:80 -v /home/vagrant/cms/wordpress:/var/www/html -d wordpress


