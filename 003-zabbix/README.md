# ZABBIX

## Instalação e Configuração do Zabbix
*Baixar o pacotedo repositorio do seu sistema opracional em https://www.zabbix.com/br/download*
 
**$ wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb**
**$ dpkg -i zabbix-release_5.0-1+focal_all.deb**
**$ apt update**

*Instalar o Zabbix*
**$ apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent**

*com banco local*
**$ mysql -uroot -p**
password
mysql> create database zabbix character set utf8 collate utf8_bin;
mysql> create user zabbix@localhost identified by 'password';
mysql> grant all privileges on zabbix.* to zabbix@localhost;
mysql> quit;

**$ zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p zabbix**


============================================================
*com bandco de dados externo, usar:*

**$ mysql -h <DB_ENDPOINT> -u<DB_USER> -p<DB_PASSWORD>**
>create database zabbix character set utf8 collate utf8_bin;
>quit;

**$ zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -h <DB_ENDPOINT> -u<DB_USER> -p<DB_PASSWORD> zabbix**
*Editar o arquivo de configuração*
**$ sudo nano /etc/zabbix/zabbix_server.conf**

DBUser=zabbix    | DBUser=<DB_USER>
DBPassword=      | DBPassword=<DB_PASS>
DBHost=localhost | DBHost=<DB_HOST>
============================================================


*No apache:*
**$ sudo nano /etc/zabbix/apache.conf**
php_value date.timezone Europe/Riga | php_value date.timezone America/Sao_Paulo

*Reiniciar o zabbix*
**$ systemctl restart zabbix-server zabbix-agent apache2**
**$ systemctl enable zabbix-server zabbix-agent apache2**


## Instalação do Agent Zabbix Linux

*$ wget http://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1%2Bfocal_all.deb*
*$ sudo dpkg -i zabbix-release_5.0-1%2Bfocal_all.deb*
*$ sudo apt update*
*$ sudo apt install zabbix-agent*
*Editar o arquivo de configuração do zabbix*
*$ sudo nano /etc/zabbix/zabbix_agentd.conf *
*nas linhas*

*Server=127.0.0.1*
*ServerActive=127.0.0.1*
*Hostname=Zabbix server*

*Colocar suas informações*

*$ sudo systemctl restart zabbix-agent*
*$ sudo systemctl enable zabbix-agent*
*$ sudo systemctl status zabbix-agent*
